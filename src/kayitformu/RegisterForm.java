package kayitformu;

import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
	

public class RegisterForm extends JPanel implements ActionListener{
	JTextField name,surname,college,faculty,department,gsm;
	JLabel label1,label2,label3,label4,label5,label6,label7;
	JButton confirm;
	static MysqlInterface sql;
	static String tableName;
	
	public RegisterForm(){
		super();
		confirm=new JButton("TAMAM");
		name=new JTextField("",15);
		surname=new JTextField("",15);
		college=new JTextField("",15);
		faculty=new JTextField("",15);
		department=new JTextField("",15);
		gsm=new JTextField("5",15);
		label1=new JLabel("*isim");
		label2=new JLabel("*soyisim");
		label3=new JLabel("*okul");
		label4=new JLabel("*fakulte");
		label5=new JLabel("*b�l�m");
		label6=new JLabel("*gsm");
		label7=new JLabel("");
	
		this.add(label1);
		this.add(name);
		this.add(label2);
		this.add(surname);
		this.add(label3);
		this.add(college);
		this.add(label4);
		this.add(faculty);
		this.add(label5);
		this.add(department);
		this.add(label6);	
		this.add(gsm);
		this.add(confirm);
		confirm.addActionListener(this);
		this.add(label7);
		
	}
	
	public static void main(String[] args) {
		JFrame frame=new JFrame("Kayit Sistemi");
		RegisterForm panel=new RegisterForm();
		frame.add(panel);
		frame.setSize(250,400);
		frame.setVisible(true);
		
		// TODO: AYARLANMASI GEREK!!!
		sql = new MysqlInterface("jdbc:mysql://localhost:3306/deneme", "root", "");
		tableName = "kayitlistesi1";
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		//long a;
		//a=Integer.parseInt(gsm.getText());
		if((e.getActionCommand()=="TAMAM") && (name.getText().equals("")  || surname.getText().equals("")  || college.getText().equals("")  || faculty.getText().equals("")  || department.getText().equals("")  || gsm.getText().equals("")  )){
			label7.setText("T�m alanlar doldurulmal�d�r");
		}
		else if((e.getActionCommand()=="TAMAM") && (surname.getText().equals(college.getText()) || college.getText().equals(faculty.getText()) || surname.getText().equals(gsm.getText()) || name.getText().equals(gsm.getText()) || surname.getText().equals(department.getText()) || name.getText().equals(college.getText()))){
			label7.setText("Alanlar� do�ru doldurunuz");
		}
		else if(gsm.getText().length() != 10){
			label7.setText("Hatali GSM! Basinda 0 olmadan GSM giriniz");
		}
		else
		{
			if (sql.send("Insert into "+ tableName +"(isim,soyisim,gsm,okul,fakulte,bolum) values ('"+ name.getText() +"','"+ surname.getText() +"','"+ gsm.getText() +"','"+ college.getText() +"','"+ faculty.getText() +"','"+ department.getText() +"')") == true)
			{
				label7.setText("Kayd�n�z al�nm��t�r");
				name.setText("");
				surname.setText("");
				gsm.setText("");
				college.setText("");
				faculty.setText("");
				department.setText("");
			}	
			else
				label7.setText("Kayit basarisiz, baglanti kurulamad�");
		}
	
		
	}

}