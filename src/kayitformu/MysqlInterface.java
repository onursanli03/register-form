package kayitformu;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MysqlInterface {

	String url, user, password;
	
	/**
	 * 
	 * @param url		"jdbc:mysql://{IP_ADDRESS}:3306/{DATABASE_NAME}"
	 * @param user		mysql user name
	 * @param password	mysql user password
	 */
	MysqlInterface(String url, String user, String password)
	{
		this.url = url;
		this.user = user;
		this.password = password;
	}
	
	/**
	 * 
	 * @param command	command to be sent ("Insert into {database}(column1,column2) values ({intvalue},'{stringvalue}')
	 * @return	returns true if successful, else returns false
	 */
	boolean send(String command)
	{
    	Connection con = null;
        Statement st = null;
        
        try {
            con = DriverManager.getConnection(url, user, password);
            st = con.createStatement();
            st.executeUpdate(command);
            
        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(MysqlInterface.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
            return false;
        }
    	
		return true;
	}
	
}
